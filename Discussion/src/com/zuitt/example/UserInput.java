package com.zuitt.example;
import  java.util.Scanner;

public class UserInput {
    public static void main (String [] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Input Number A to add: ");
    int x = scan.nextInt();
    System.out.print("Input Number B to add: ");
    int y = scan.nextInt();
    System.out.println("A + B = "+ (x+y));

    System.out.print("Enter your First Name: ");
    scan.nextLine();
    String firstName = scan.nextLine();
    System.out.print("Enter your Last Name: ");
    String lastName = scan.nextLine();
    System.out.println("Hello "+ (firstName + " " + lastName) + " \n* message from Java");

    }
}
