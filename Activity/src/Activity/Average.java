package Activity;
import java.util.Scanner;
import java.text.DecimalFormat;

public class Average {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        DecimalFormat format = new DecimalFormat("0.#");

        String fName;
        String lName;
        double subject1;
        double subject2;
        double subject3;

        System.out.print("First Name: ");
        fName = scan.nextLine();
        System.out.print("Name: ");
        lName = scan.nextLine();

        System.out.print("First Subject Grade: ");
        subject1 = scan.nextDouble();
        System.out.print("Second Subject Grade: ");
        subject2 = scan.nextDouble();
        System.out.print("Third Subject Grade: ");
        subject3 = scan.nextDouble();

        double sum = subject1 + subject2 + subject3;
        double average = Math.round(sum/3);

        System.out.println("Good Day, "+ fName + " " + lName + ".");
        System.out.println("Your Grade Average is: "+ format.format(average));
    }
}